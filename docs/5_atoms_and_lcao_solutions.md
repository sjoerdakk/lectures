# Solutions for LCAO model exercises

### Exercise 1

#### Question 1.
See lecture notes.
#### Question 2.
The atomic number of Tungsten is 74:

$$
1s^22s^22p^63s^23p^64s^23d^{10}4p^65s^24d^{10}5p^66s^24f^{14}5d^4
$$

#### Question 3.

\begin{align}
\textrm{Cu} &= [\textrm{Ar}]4s^23d^9\\
\textrm{Pd} &= [\textrm{Kr}]5s^24d^8\\
\textrm{Ag} &= [\textrm{Kr}]5s^24d^9\\
\textrm{Au} &= [\textrm{Xe}]6s^24f^{14}5d^9
\end{align}

### Exercise 2
#### Question 1.

$$
\psi(x) =
\begin{cases}
    &\sqrt{κ}e^{κ(x-x_1)}, x<x_1\\
    &\sqrt{κ}e^{-κ(x-x_1)}, x>x_1
\end{cases}
$$

Where $κ = \sqrt{\frac{-2mE}{\hbar^2}} = \frac{mV_0}{\hbar^2}$.
The energy is given by $ϵ_1 = ϵ_2 = -\frac{mV_0^2}{2\hbar^2}$
The wave function of a single delta peak is given by

$$
    \psi_1(x) = \frac{\sqrt{mV_0}}{\hbar}e^{-\frac{mV_0}{\hbar^2}|x-x_1|}
$$

$\psi_2(x)$ can be found by replacing $x_1$ by $x_2$

#### Question 2.

$$
H = -\frac{mV_0^2}{\hbar^2}\begin{pmatrix}
    1/2+\exp(-\frac{2mV_0}{\hbar^2}|x_2-x_1|) &
    \exp(-\frac{mV_0}{\hbar^2}|x_2-x_1|)\\
    \exp(-\frac{mV_0}{\hbar^2}|x_2-x_1|) &
    1/2+\exp(-\frac{2mV_0}{\hbar^2}|x_2-x_1|)
\end{pmatrix}
$$

#### Question 3.

$$
ϵ_{\pm} = \beta(1/2+\exp(-2\alpha) \pm \exp(-\alpha))
$$

Where $\beta = -\frac{mV_0^2}{\hbar^2}$ and $α = \frac{mV_0}{\hbar^2}|x_2-x_1|$

### Exercise 3

#### Question 1.

$$
    \hat{H}_{\mathcal{E}} = e\mathcal{E}\hat{x},
$$

#### Question 2.

$$
    \hat{H} = \begin{pmatrix}
    E_0  & -t \\
    -t   & E_0 
    \end{pmatrix} +e\mathcal{E}\begin{pmatrix}
    \langle 1|\hat{x}|1\rangle & \langle 1|\hat{x}|2\rangle \\
    \langle 2|\hat{x}|1\rangle & \langle 2|\hat{x}|2\rangle
    \end{pmatrix} = \begin{pmatrix}
    E_0-\gamma & -t \\
    -t & E_0+\gamma
    \end{pmatrix},
$$
where we defined $\gamma = e d \mathcal{E}/2$ and used 
$$
    \langle 1|\hat{x}|1\rangle = -\frac{d}{2}.
$$

#### Question 3.

The eigenstates of the Hamiltonian are given by:
$$
    E_{\mp} = E_0\pm\sqrt{t^2+\gamma^2}
$$
Calling the elements of the eigenvector $\phi_1$ and $\phi_2$, we find
$$
\phi_1(E_0-\gamma)-\phi_2 t = \phi_1 E_\mp
$$
From this we find for the ground state 
$$
\phi_2 = -\frac{E_+- E_0 + \gamma}{t}\phi_1 = \frac{\sqrt{t^2+ \gamma^2} - \gamma }{t}\phi_1 
$$

For simplicity, we now assume that the electric field is small, such that $\gamma/t=\eta \ll 1$. We get
$$
\phi_2 \approx (1-\eta)\phi_1 
$$
Then, using the normalization condition $\phi_1^2+\phi_2^2$=1, we find 
$$
\phi_1\approx\frac{1}{\sqrt{2(1-\eta)}}
$$

#### Question 4.
We find the polarization using 
$$
P=2e\langle\psi|\hat{x}|\psi\rangle = 2e\left(\phi_1^2\langle 1|\hat{x}| 1 \rangle + \phi_2^2\langle 2|\hat{x}| 2 \rangle \right) = ed(\phi_2^2-\phi_1^2) =ed\frac{\eta}{1-\eta}
$$
Does the answer make sense for $\eta=0$?
